#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#define X 10
#define Y 18
#define SIZE 1024
#define PATH_ORIGINAL "tests/original_res"
#define PATH_S21 "tests/s21_res"
#define S21 "s21_cat"
#define ORIGINAL "cat"
#define TEST_FILE "tests/test.txt"

typedef unsigned long marker;
marker one = 1;

int my_fclose(FILE* f) {
    int res = 0;
    if (f != NULL) {
        fclose(f);
        res = 1;
    }
    return res;
}

void my_cmp(char opt_str[SIZE], int* cmp, int* cmp_failed, char fname[X * Y]) {
    int ch1 = 0, ch2 = 0;
    int line = 1;
    int nchar = 0;
    char fname1[SIZE] = {0};
    char fname2[SIZE] = {0};
    snprintf(fname1, sizeof(fname1), "%s/%s.txt", PATH_ORIGINAL, fname);
    snprintf(fname2, sizeof(fname2), "%s/%s.txt", PATH_S21, fname);
    FILE* f1 = fopen(fname1, "r");
    FILE* f2 = fopen(fname2, "r");
    if (f1 != NULL && f2 != NULL) {
        while (!feof(f1) && !feof(f2) && (ch1 == ch2)) {
            ch1 = fgetc(f1);
            ch2 = fgetc(f2);
            nchar++;
            if ((ch1 == '\n') && (ch2 == '\n')) {
                nchar = 1;
                line++;
            }
        }
        if (!(ch1 == ch2)) {
            printf("Options \"%s\" test failed on line %d, char %d. cat=\"%c\" s21_cat=\"%c\"\n",
                    opt_str, line, nchar, ch1, ch2);
            (*cmp_failed)++;
        } else {
            (*cmp)++;
        }
        my_fclose(f2);
        my_fclose(f1);
    }
}

void comb(int pool, int need, marker chosen, int at, char opts[X][Y], int v_prog, int* cmp, int* cmp_failed) {
    if (pool < need + at) return;
    if (!need) {
        char opt_str[X * Y] = {0};
        char fname[X * Y] = {0};
        for (at = 0; at < pool; at++)
            if (chosen & (one << at)) {
                if (strlen(opt_str) > 0) {
                    snprintf(opt_str + strlen(opt_str), sizeof(opt_str), " %s", opts[at]);
                    if ((strcmp("-T", opts[at]) == 0) || (strcmp("-E", opts[at]) == 0)) {
                        snprintf(fname + strlen(fname), sizeof(fname), " %s1", opts[at]);
                    } else {
                        snprintf(fname + strlen(fname), sizeof(fname), " %s", opts[at]);
                    }
                } else {
                    snprintf(opt_str + strlen(opt_str), sizeof(opt_str), "%s", opts[at]);
                    if ((strcmp("-T", opts[at]) == 0) || (strcmp("-E", opts[at]) == 0)) {
                        snprintf(fname + strlen(fname), sizeof(fname), "%s1", opts[at]);
                    } else {
                        snprintf(fname + strlen(fname), sizeof(fname), "%s", opts[at]);
                    }
                }
            }
        char cmd[SIZE] = {0};
        if (v_prog == 0) {
            snprintf(cmd, sizeof(cmd), "%s %s %s %s > %s/\"%s.txt\"",
                    ORIGINAL, opt_str,  TEST_FILE, TEST_FILE, PATH_ORIGINAL, fname);
            system(cmd);
        }
        if (v_prog == 1) {
            snprintf(cmd, sizeof(cmd), "./%s %s %s %s > \"%s/%s.txt\"", S21,
                    opt_str, TEST_FILE,  TEST_FILE, PATH_S21, fname);
            system(cmd);
            my_cmp(opt_str, cmp, cmp_failed, fname);
        }
        return;
    }
    comb(pool, need - 1, chosen | (one << at), at + 1, opts, v_prog, cmp, cmp_failed);
    comb(pool, need, chosen, at + 1, opts, v_prog, cmp, cmp_failed);
}

void make_original(char opts[X][Y]) {
    for (int i = 1; i < X + 1; i++) {
        comb(X, i, 0, 0, opts, 0, 0, 0);
    }
    char cmd[SIZE] = {0};
    snprintf(cmd, sizeof(cmd), "tar -cf %s/original_res.tar %s", PATH_ORIGINAL, PATH_ORIGINAL);
    system(cmd);
}

void make_cmp(char opts[X][Y], int* cmp, int* cmp_failed) {
    printf("Tests started\n");
    for (int i = 1; i < X + 1; i++) {
        comb(X, i, 0, 0, opts, 1, cmp, cmp_failed);
    }
}

int my_untar() {
    int res = 0;
    char fname[SIZE] = {0};
    snprintf(fname, sizeof(fname), "%s/%s.tar", PATH_ORIGINAL, "original_res");
    FILE* f = fopen(fname, "rb");
    if (my_fclose(f)) {
        char cmd[SIZE] = "tar -xf ";
        snprintf(cmd + strlen(cmd), sizeof(cmd), "%s", fname);
        system(cmd);
        res = 1;
    }
    return res;
}

int main(int argc, char* argv[]) {
    FILE* tfile = fopen(TEST_FILE, "r");
    if (!my_fclose(tfile)) {
        printf("No file %s for test. Its can be some truble...\n", TEST_FILE);
        return -1;
    }
    system("mkdir -p "PATH_S21);
    system("mkdir -p "PATH_ORIGINAL);
    int cmp = 0;
    int cmp_failed = 0;
    int rez = 0;
    char opts[X][Y] = {{"-b"}, {"-e"}, {"-E"}, {"-n"}, {"-s"}, {"-t"}, {"-T"},
                        {"--number-nonblank"}, {"--number"}, {"--squeeze-blank"}};
    while ((rez = getopt(argc, argv, "cz")) != -1) {
        if (rez == 'c') {
            make_original(opts);
        }
        if (rez == 'z') {
            my_untar();
        }
    }
    make_cmp(opts, &cmp, &cmp_failed);
    printf("%d Test. Success: %d. Failed: %d.\n", cmp + cmp_failed, cmp, cmp_failed);
    if ((cmp + cmp_failed) == 0) {
        printf("Trying to untar original results\n");
        if (my_untar() == 0) {
            printf("No tar with original results. Make this using -c\n");
        } else {
            printf("Successful. Trying tests again.\n");
            make_cmp(opts, &cmp, &cmp_failed);
            printf("%d Test. Completed: %d. Failed: %d.\n", cmp + cmp_failed, cmp, cmp_failed);
        }
    }
    return 0;
}
