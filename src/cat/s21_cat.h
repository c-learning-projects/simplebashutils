#ifndef SRC_CAT_S21_CAT_H_
#define SRC_CAT_S21_CAT_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>

#define FLAG_b (1U << 1U)
#define FLAG_E (1U << 2U)
#define FLAG_n (1U << 3U)
#define FLAG_s (1U << 4U)
#define FLAG_T (1U << 5U)
#define FLAG_v (1U << 6U)

int main(int argc, char *argv[]);
void make_flags(int argc, char *argv[]);
FILE* file_open(char* fname);
void catprint(FILE* file, unsigned flags, unsigned* row_counter, unsigned* new_file);
void catprintstr(char* cstr, unsigned flags, unsigned* row_counter, unsigned* empty_str, unsigned* new_file);
void numerator(char** cstr, unsigned* row_counter);
void myfree(void** ptr);
#endif  // SRC_CAT_S21_CAT_H_
