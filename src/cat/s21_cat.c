#include "s21_cat.h"

int main(int argc, char *argv[]) {
    make_flags(argc, argv);
    return 0;
}

void make_flags(int argc, char *argv[]) {
    unsigned flags = 0;
    unsigned row_counter = 1;
    unsigned new_file = 0;
    FILE* file = NULL;
    const char* short_options = "beEnstTv";
    const struct option long_options[] = {
        {"number-nonblank", no_argument, NULL, 'b'},
        {"number", no_argument, NULL, 'n'},
        {"squeeze-blank", no_argument, NULL, 's'},
        {NULL, 0 , NULL, 0}
    };
    int rez;
    int option_index;
    while ((rez = getopt_long(argc, argv, short_options, long_options, &option_index)) != -1) {
         if (rez == short_options[0]) {
            flags |= FLAG_b;
            } else if (rez == short_options[1]) {
                flags |= FLAG_E;
                flags |= FLAG_v;
            } else if (rez == short_options[2]) {
                flags |= FLAG_E;
            } else if (rez == short_options[3]) {
                flags |= FLAG_n;
            } else if (rez == short_options[4]) {
                flags |= FLAG_s;
            } else if (rez == short_options[5]) {
                flags |= FLAG_T;
                flags |= FLAG_v;
            } else if (rez == short_options[6]) {
                flags |= FLAG_T;
            } else if (rez == short_options[7]) {
                flags |= FLAG_v;
        }
    }
    if ((flags & FLAG_b) && (flags & FLAG_n)) {
        flags &= ~FLAG_n;
    }

    while (optind != argc) {
        if ((file = file_open(argv[optind])) != NULL) {
            new_file = 1;
            catprint(file, flags, &row_counter, &new_file);
            fclose(file);
        }
        optind++;
    }
}

void catprint(FILE* file, unsigned flags, unsigned* row_counter, unsigned* new_file) {
    unsigned size = 0;
    char ch;
    char chrs[5];
    unsigned empty_str = 0;
    char* cstr = malloc(1);
    cstr[0] = '\0';
    while (fread(&ch, sizeof(char), 1, file)) {
        if ((ch < -96) && (flags & FLAG_v)) {
            snprintf(chrs, sizeof(chrs), "M-^%c", ch + 192);
        } else if ((ch == -1) && (flags & FLAG_v)) {
            snprintf(chrs, sizeof(chrs), "%s", "M-^?");
        } else if ((ch < -1) && (flags & FLAG_v)) {
            snprintf(chrs, sizeof(chrs), "M-%c", ch + 128);
        } else if ((ch == 10) && (flags & FLAG_E)) {
            snprintf(chrs, sizeof(chrs), "%s", "$\n");
        } else if ((ch == 9) && (flags & FLAG_T)) {
            snprintf(chrs, sizeof(chrs), "%s", "^I");
        } else if (ch == 9 || ch == 10) {
            snprintf(chrs, sizeof(chrs), "%c", ch);
        } else if ((ch < 32) && (flags & FLAG_v)) {
            snprintf(chrs, sizeof(chrs), "^%c", ch + 64);
        } else if (ch < 127) {
            snprintf(chrs, sizeof(chrs), "%c", ch);
        } else if (flags & FLAG_v) {
            snprintf(chrs, sizeof(chrs), "%s", "^?");
        }
        if ((strlen(cstr) + strlen(chrs) + 10) > size) {
            size = (strlen(cstr) + strlen(chrs) + 10);
        }
        cstr = realloc(cstr, size * sizeof(char));
        snprintf(cstr + strlen(cstr), sizeof(cstr), "%s", chrs);
        cstr[size - 1] = '\0';
        if (strchr(chrs, '\n') != NULL) {
            catprintstr(cstr, flags, row_counter, &empty_str, new_file);
            cstr[0] = '\0';
        }
    }
    catprintstr(cstr, flags, row_counter, &empty_str, new_file);
    free(cstr);
    cstr = NULL;
}

void catprintstr(char* cstr, unsigned flags, unsigned* row_counter,
                unsigned* empty_str_counter, unsigned* new_file) {
    unsigned print = 0;
    unsigned empty_str = 0;
    unsigned make_num = 1;
    if ((strcmp(cstr, "") == 0) || (strcmp(cstr, "\n") == 0)
        || ((flags & FLAG_E) && (((strcmp(cstr, "$\n") == 0)) || ((strcmp(cstr, "$") == 0)))))
        empty_str = 1;
    if ((flags & FLAG_s) && empty_str) {
        (*empty_str_counter)++;
    } else {
        (*empty_str_counter) = 0;
    }
    if (*new_file && (*row_counter > 1)) make_num = 0;
    if (((flags & FLAG_s) && (*empty_str_counter < 2)) || !(flags & FLAG_s)) print = 1;
    if ((flags & FLAG_n) && print && make_num) numerator(&cstr, row_counter);
    if ((flags & FLAG_b) && !(empty_str) && print && make_num) numerator(&cstr, row_counter);
    if (print) printf("%s", cstr);
    *new_file = 0;
}

void numerator(char** cstr, unsigned* row_counter) {
    char* temp = NULL;
    temp = malloc((10 + strlen(*cstr)) * sizeof(char));
    if (temp != NULL) {
        snprintf(temp, (10 + strlen(*cstr)), "%6d\t%s", *row_counter, *cstr);
        snprintf(*cstr, (10 + strlen(*cstr)), "%s", temp);
        free(temp);
        temp = NULL;
        (*row_counter)++;
    }
}

FILE* file_open(char* fname) {
    FILE* file = NULL;
    if (((file = fopen(fname, "rb+")) == NULL) & (errno != 26)) {
        fprintf(stderr, "%s: %s: %s\n", "s21_cat", fname, strerror(errno));
    }
    if (file != NULL) fclose(file);
    file = fopen(fname, "rb");
    return file;
}
