#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#define X 10
#define Y 20
#define SIZE 1024
#define PATH_ORIGINAL "tests/original_res"
#define PATH_S21 "tests/s21_res"
#define S21 "s21_grep"
#define ORIGINAL "grep"
#define TEST_FILE "tests/Makefile.txt"
#define TEST_FILE2 "tests/s21_grep.txt"
#define TEST_FILE_REG "tests/test.txt"

typedef unsigned long marker;
marker one = 1;

int my_fclose(FILE* f) {
    int res = 0;
    if (f != NULL) {
        fclose(f);
        res = 1;
    }
    return res;
}

void my_cmp(char opt_str[SIZE], int* cmp, int* cmp_failed, char* fname) {
    int ch1 = 0, ch2 = 0;
    int line = 1;
    int nchar = 0;
    char fname1[SIZE] = {0};
    char fname2[SIZE] = {0};
    snprintf(fname1, sizeof(fname1), "%s/%s.txt", PATH_ORIGINAL, fname);
    snprintf(fname2, sizeof(fname2), "%s/%s.txt", PATH_S21, fname);
    FILE* f1 = fopen(fname1, "r");
    FILE* f2 = fopen(fname2, "r");
    if (f1 != NULL && f2 != NULL) {
        while (!feof(f1) && !feof(f2) && (ch1 == ch2)) {
            ch1 = fgetc(f1);
            ch2 = fgetc(f2);
            nchar++;
            if ((ch1 == '\n') && (ch2 == '\n')) {
                nchar = 1;
                line++;
            }
        }
        if (!(ch1 == ch2)) {
            printf("Options \"%s\" - test failed on line %d, char %d. grep=\"%c\" s21_grep=\"%c\"\n",
                    opt_str, line, nchar, ch1, ch2);
            (*cmp_failed)++;
        } else {
            (*cmp)++;
        }
        my_fclose(f2);
        my_fclose(f1);
    }
}

void comb(int pool, int need, marker chosen, int at, char opts[X][Y], int v_prog, int* cmp, int* cmp_failed) {
    if (pool < need + at) return;
    int ise = 0;
    int isf = 0;
    char fname[X * Y + 16] = {0};
    if (!need) {
        char opt_str[X * Y] = {0};
        for (at = 0; at < pool; at++)
            if (chosen & (one << at)) {
                if (strlen(opt_str) > 0) {
                    snprintf(opt_str + strlen(opt_str), sizeof(opt_str), " %s", opts[at]);
                    snprintf(fname + strlen(fname), sizeof(fname), " %s", opts[at]);
                } else {
                    snprintf(opt_str + strlen(opt_str), sizeof(opt_str), "%s", opts[at]);
                    snprintf(fname + strlen(fname), sizeof(fname), "%s", opts[at]);
                }
                if (strcmp(opts[at], "-e") == 0) {
                    snprintf(opt_str + strlen(opt_str), sizeof(opt_str), " \\Q+-*/\\E");
                    ise = 1;
                }
                if (strcmp(opts[at], "-f") == 0) {
                    snprintf(opt_str + strlen(opt_str), sizeof(opt_str), " %s", TEST_FILE_REG);
                    isf = 1;
                }
            }
        char cmd[SIZE] = {0};
        if (v_prog == 0) {
            if (ise || isf) {
                snprintf(cmd, sizeof(cmd), "%s %s %s %s > \"%s/%s.txt\"",
                            ORIGINAL, opt_str, TEST_FILE, TEST_FILE2, PATH_ORIGINAL, fname);
            } else {
                snprintf(cmd, sizeof(cmd), "%s %s 21 %s %s > \"%s/%s.txt\"",
                            ORIGINAL, opt_str, TEST_FILE, TEST_FILE2, PATH_ORIGINAL, fname);
            }
            system(cmd);
        }
        if (v_prog == 1) {
            if (ise || isf) {
            snprintf(cmd, sizeof(cmd), "./%s %s %s %s > \"%s/%s.txt\"",
                        S21, opt_str, TEST_FILE, TEST_FILE2, PATH_S21, fname);
            } else {
                snprintf(cmd, sizeof(cmd), "./%s %s 21 %s %s > \"%s/%s.txt\"",
                        S21, opt_str, TEST_FILE, TEST_FILE2, PATH_S21, fname);
            }
            system(cmd);
            my_cmp(cmd, cmp, cmp_failed, fname);
        }
        return;
    }
    comb(pool, need - 1, chosen | (one << at), at + 1, opts, v_prog, cmp, cmp_failed);
    comb(pool, need, chosen, at + 1, opts, v_prog, cmp, cmp_failed);
}

void make_original(char opts[X][Y]) {
    for (int i = 1; i < X + 1; i++) {
        comb(X, i, 0, 0, opts, 0, 0, 0);
    }
    char cmd[SIZE] = {0};
    snprintf(cmd, sizeof(cmd), "tar -cf %s/original_res.tar %s", PATH_ORIGINAL, PATH_ORIGINAL);
    system(cmd);
}

void make_cmp(char opts[X][Y], int* cmp, int* cmp_failed) {
    printf("Tests started\n");
    for (int i = 1; i < X + 1; i++) {
        comb(X, i, 0, 0, opts, 1, cmp, cmp_failed);
    }
}

int my_untar() {
    int res = 0;
    char fname[SIZE] = {0};
    snprintf(fname, sizeof(fname), "%s/%s.tar", PATH_ORIGINAL, "original_res");
    FILE* f = fopen(fname, "rb");
    if (my_fclose(f)) {
        char cmd[SIZE] = "tar -xf ";
        snprintf(cmd + strlen(cmd), sizeof(cmd), "%s", fname);
        system(cmd);
        res = 1;
    }
    return res;
}

int main(int argc, char* argv[]) {
    FILE* tfile = fopen(TEST_FILE, "r");
    FILE* tfile2 = fopen(TEST_FILE2, "r");
    if (!my_fclose(tfile) || !my_fclose(tfile2)) {
        printf("No file %s for test. Its can be some truble...\n", TEST_FILE);
        return -1;
    }
    system("mkdir -p "PATH_S21);
    system("mkdir -p "PATH_ORIGINAL);
    int cmp = 0;
    int cmp_failed = 0;
    int rez = 0;
    char opts[X][Y] = {{"-e"}, {"-i"}, {"-v"}, {"-c"}, {"-l"}, {"-n"}, {"-h"},
                        {"-s"}, {"-f"}, {"-o"}};
    while ((rez = getopt(argc, argv, "cz")) != -1) {
        if (rez == 'c') {
            make_original(opts);
        }
        if (rez == 'z') {
            my_untar();
        }
    }
    make_cmp(opts, &cmp, &cmp_failed);
    printf("%d Test. Success: %d. Failed: %d.\n", cmp + cmp_failed, cmp, cmp_failed);
    if ((cmp + cmp_failed) == 0) {
        printf("Trying to untar original results\n");
        if (my_untar() == 0) {
            printf("No tar with original results. Make this using -c\n");
        } else {
            printf("Successful. Trying tests again.\n");
            make_cmp(opts, &cmp, &cmp_failed);
            printf("%d Test. Completed: %d. Failed: %d.\n", cmp + cmp_failed, cmp, cmp_failed);
        }
    }
    return 0;
}
