#include "s21_grep.h"

int main(int argc, char* argv[]) {
    make_flags(argc, argv);
    return 0;
}

void make_flags(int argc, char *argv[]) {
    FILE* file_opt_f = NULL;
    FILE* file = NULL;
    int rez = 0;
    unsigned flags = 0;
    const char* short_options = "e:ivclnhsof:";
    char reg[LEN] = {0};
    while ((rez = getopt(argc, argv, short_options)) != -1) {
        if (rez == short_options[0]) {
            if (!strlen(reg)) {
                snprintf(reg, LEN, "%s", optarg);
            } else {
                snprintf(reg + strlen(reg),
                LEN - strlen(reg), "|%s", optarg);
            }
        } else if (rez == short_options[2]) {
            flags |= FLAG_i;
        } else if (rez == short_options[3]) {
            flags |= FLAG_v;
        } else if (rez == short_options[4]) {
            flags |= FLAG_c;
        } else if (rez == short_options[5]) {
            flags |= FLAG_l;
        } else if (rez == short_options[6]) {
            flags |= FLAG_n;
        } else if (rez == short_options[7]) {
            flags |= FLAG_h;
        } else if (rez == short_options[8]) {
            flags |= FLAG_s;
        } else if (rez == short_options[9]) {
            flags |= FLAG_o;
        } else if (rez == short_options[10]) {
            flags |= FLAG_f;
            if ((file_opt_f = file_open(optarg, flags)) != NULL) {
                reg_from_files(file_opt_f, reg);
                fclose(file_opt_f);
            }
        }
    }
    if ((flags & FLAG_c) && (flags & FLAG_o) && (flags & FLAG_v)) flags &= ~FLAG_o;
    if ((flags & FLAG_v) && (flags & FLAG_l) && (flags & FLAG_o)) flags &= ~FLAG_o;
    if ((flags & FLAG_i) && (flags & FLAG_l) && (flags & FLAG_o)) flags &= ~FLAG_o;
    if ((optind + 1 == argc) || (!strlen(reg) && (optind + 2 == argc))) {
        flags |= FLAG_h;
    }
    while (optind != argc) {
        if (!strlen(reg)) {
            snprintf(reg, LEN, "%s", argv[optind]);
        } else {
            if ((file = file_open(argv[optind], flags)) != NULL) {
                getprint(file, flags, reg, argv[optind]);
                fclose(file);
            }
        }
        optind++;
    }
}

void getprint(FILE* file, unsigned flags, char reg[LEN], char* fname) {
    char* str = NULL;
    int reg_res = 0;
    size_t len_str = 0;
    ssize_t getline_res = 0;
    size_t str_counter = 0;
    int my_breake = 0;
    size_t str_cmp_counter = 0;
    while ((getline_res = getline(&str, &len_str , file)) != -1 && str && (my_breake == 0)) {
        str_counter++;
        reg_res = make_reg(str, reg, flags, fname, str_counter);
        if (reg_res == -1) {
             my_breake = -1;
        } else if ((reg_res == 0) && !(flags & FLAG_v)) {
            if (!(flags & FLAG_o) && !(flags & FLAG_c))
                print_str(str, fname, str_counter, flags, strlen(str), &my_breake);
            str_cmp_counter++;
        } else if ((reg_res == 1) && (flags & FLAG_v)) {
            if (!(flags & FLAG_o) && !(flags & FLAG_c)) print_str(str, fname, str_counter,
                    flags, strlen(str), &my_breake);
            str_cmp_counter++;
        }
    }
    if (my_breake != -1) {
        print_str("\0", fname, str_cmp_counter, flags, -1, &my_breake);
    }
    my_free(str);
}

void print_str(char* str, char* fname, size_t str_counter, unsigned flags, int len, int* my_breake) {
    if (((flags & FLAG_v) && (flags & FLAG_o)) || ((len == -1) && (flags & FLAG_l) && (str_counter == 0)))
        return;
    if ((flags & FLAG_l) && (str_counter > 0)) {
        (*my_breake) = -1;
        printf("%s\n", fname);
        return;
    } else if ((!(flags & FLAG_h) && (len != -1)) || ((FLAG_c & flags) && !(flags & FLAG_h))) {
            printf("%s:", fname);
    }
    if ((flags & FLAG_c) && (len == -1)) {
        printf("%zu\n", str_counter);
    } else if (len != -1) {
        if (flags & FLAG_n) printf("%zu:", str_counter);
        printf("%.*s", len, str);
        if ((len > 0) && (str[len - 1] != '\n')) printf("\n");
    }
}

void reg_from_files(FILE* file, char reg[LEN]) {
    char* str = NULL;
    size_t l_of_str = 0;
    ssize_t n_of_str = 0;
    if (!strlen(reg)) {
        if ((n_of_str = getline(&str, &l_of_str , file)) != -1 && str) {
            if (n_of_str > 1 && str[n_of_str - 1] == '\n') {
                str[n_of_str - 1] = '\0';
            }
            snprintf(reg, LEN, "%s", str);
        }
    }
    while ((n_of_str = getline(&str, &l_of_str , file)) != -1 && str) {
        if (n_of_str > 1  && str[n_of_str - 1] == '\n') {
            str[n_of_str - 1] = '\0';
        }
        snprintf(reg + strlen(reg), LEN - strlen(reg), "|%s", str);
    }
    my_free(str);
}

int make_reg(char* str, char* reg, unsigned flags, char* fname, size_t str_counter) {
    int result = -2;
    regex_t rexp;
    char buffer[100];
    int res_val;
    if (flags & FLAG_i) {
        res_val = regcomp(&rexp, reg, REG_EXTENDED | REG_NEWLINE | REG_ICASE);
    } else {
        res_val = regcomp(&rexp, reg, REG_EXTENDED | REG_NEWLINE);
    }
    if (res_val) {
        regerror(res_val, &rexp, buffer, 100);
        fprintf(stderr, "s21_grep: %s\n", buffer);
        result = -1;
    } else {
        regmatch_t pmatch;
        if (!(flags & FLAG_o)) {
            result = regexec(&rexp, str, 1, &pmatch, 0);
        } else {
            char* temp_str = str;
            while (!regexec(&rexp, temp_str, 1, &pmatch, 0)) {
                result = 0;
                if (!(flags & FLAG_c)) {
                        print_str(temp_str + pmatch.rm_so, fname, str_counter, flags,
                            (int)(pmatch.rm_eo - pmatch.rm_so), &result);
                }
                temp_str += pmatch.rm_eo;
            }
        }
        regfree(&rexp);
    }
    return result;
}

void my_free(char* str) {
    if (str != NULL) {
        free(str);
        str = NULL;
    }
}

FILE* file_open(char* fname, unsigned flags) {
    FILE* file = NULL;
    if (((file = fopen(fname, "r+")) == NULL) & (errno != 26)) {
        if (!(flags & FLAG_s))
            fprintf(stderr, "%s: %s: %s\n", "s21_grep", fname, strerror(errno));
    }
    if (file != NULL) {
        fclose(file);
        file = fopen(fname, "r");
    }
    return file;
}
